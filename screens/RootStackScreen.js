import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from './SplashScreen';
import SignInScreen from './SignInScreen';
import SignUpScreen from './SignUpScreen';

import KlaimScreen from './KlaimScreen';
import NewKlaimScreen from './NewKlaimScreen';
import EditKlaimScreen from './EditKlaimScreen';

import EmployeeScreen from './EmployeeScreen';
import NewEmployeeScreen from './NewEmployeeScreen';
import EditEmployeeScreen from './EditEmployeeScreen';

import SemuaStatusKlaim from './semuaStatusKlaim/index';
import RiwayatPengajuan from './riwayatPengajuan/index';
import Profil from './Profil/Edit/index';
import PageDiSetujui from './PageDisetujui/pageDisetujui';
import Notif from './Notif/ListNotif';
import KepalaBagian from './KepalaBagian/detailPengajuan';
import Kabag from './Kabag/Home/index';
import HRD from './HRD/Home/index';
import Home from './Home/index';
import DetailPengajuanDiProses from './detailPengajuan/Diproses';
import Daftar from './Daftar/index';
import Akun from './Akun/Akun';
import Ajuan from './Ajuan/index';
import Admin from './Admin/Home';
import ListNotif from './Notif/ListNotif/index';
import DetailNotif from './Notif/DetailNotif/index';
import RiwayatMingguIni from './riwayatPengajuan/mingguIni/index';
import RiwayatBulanIni from './riwayatPengajuan/bulanIni/index';
import ProfilFix from './Profil/Fix/index';




const RootStack = createStackNavigator();

const RootStackScreen = ({navigation}) => (
    <RootStack.Navigator headerMode='none'>

        <RootStack.Screen name="SplashScreen" component={SplashScreen}/>
        <RootStack.Screen name="SignInScreen" component={SignInScreen}/>
        <RootStack.Screen name="SignUpScreen" component={SignUpScreen}/>

        <RootStack.Screen name="CompanyScreen" component={KlaimScreen}/>
        <RootStack.Screen name="NewCompanyScreen" component={NewKlaimScreen}/>
        <RootStack.Screen name="EditCompanyScreen" component={EditKlaimScreen}/>

        <RootStack.Screen name="EmployeeScreen" component={EmployeeScreen}/>
        <RootStack.Screen name="NewEmployeeScreen" component={NewEmployeeScreen}/>
        <RootStack.Screen name="EditEmployeeScreen" component={EditEmployeeScreen}/>

        <RootStack.Screen name="SemuaStatusKlaimScreen" component={SemuaStatusKlaim}/>
        <RootStack.Screen name="RiwayatPengajuanScreen" component={RiwayatPengajuan}/>
        <RootStack.Screen name="home" component={Home}/>

        <RootStack.Screen name="ListNotif" component={ListNotif}/>
        <RootStack.Screen name="DetailNotif" component={DetailNotif}/>
        <RootStack.Screen name="detailPengajuanDiproses" component={DetailPengajuanDiProses}/>
        <RootStack.Screen name="listKlaim" component={SemuaStatusKlaim}/>

        <RootStack.Screen name="ajuan" component={Ajuan}/>
        <RootStack.Screen name="riwayat" component={RiwayatPengajuan}/>

        <RootStack.Screen name="riwayat_mingguIni" component={RiwayatMingguIni}/>
        <RootStack.Screen name="riwayat_bulanIni" component={RiwayatBulanIni}/>
        
        <RootStack.Screen name="profilFix" component={ProfilFix}/>

    </RootStack.Navigator>
);

export default RootStackScreen;