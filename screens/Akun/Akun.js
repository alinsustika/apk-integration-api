import React, {Component} from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  YellowBox,
  TextInput,
  ScrollView,
  ImageBackground,
} from "react-native";
// import cardHome from "../../assets/ch.png";
import profil from "../../assets/man.jpg";
// import cardInfoApp from "../../assets/card_infoApp.jpg";

export default class HomeActivity extends Component {
  constructor(props) {
    super(props);
    YellowBox.ignoreWarnings([
      "Warning: isMounted(...) is deprecated",
      "Module RCTImageLoader",
    ]);
  }

  render() {
    return (
      <ScrollView>
        <View style={{marginLeft: 25, backgroundColor: "white"}}>
          <View style={{flexDirection: "row"}}>
            <Text style={[styles.text, {fontSize: 24}]}>Akun</Text>
            <Text style={[styles.text, {fontSize: 24}]}>Edit</Text>
          </View>
          <Image source={profil} style={styles.profil} />

          <Text style={styles.SubJudul}>Nama</Text>
          <TextInput style={styles.TextInput}>Faiz Rendi Verdian</TextInput>
          <Text style={styles.SubJudul}>Email</Text>
          <TextInput style={styles.TextInput}>rendyan@bluework.com</TextInput>
          <Text style={styles.SubJudul}>Posisi</Text>
          <TextInput style={styles.TextInput}>Product Manager</TextInput>

          <View>
            <View style={{flexDirection: "row"}}>
              <Image source={card_infoApp} />
              <Text style={styles.ambilFoto}>Info Aplikasi</Text>

              <Image source={card_infoApp} />
              <Text style={styles.ambilFoto}>
                Ketentuan dan Kebijakan privasi
              </Text>
            </View>
            <Text>Logout</Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  cardHome: {
    marginTop: 1,
    width: 308,
    height: 190,
  },
  text: {
    color: "#1F1F1F",
    marginLeft: 26,
    marginTop: 12,
    fontSize: 12,
    fontFamily: "Poppins-Regular",
  },
  status: {
    marginTop: 10,
    marginLeft: -25,
    fontFamily: "Poppins-Regular",
    fontSize: 16,
  },
  cardStatus: {
    flexDirection: "column",
    marginLeft: 24,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 308,
    height: 108,
  },
  cardStatusKolom1: {
    marginLeft: 10,
    marginTop: 10,
    color: "black",
  },
  cardStatusKolom2: {
    marginLeft: 220,
    marginTop: -20,
  },
  profil: {
    width: 140,
    height: 140,
    borderRadius: 50,
    marginLeft: 250,
    marginTop: 35,
  },
  TextInput: {
    backgroundColor: "#F4F4F4",
    width: 292,
    height: 55,
    marginLeft: 34,
    borderRadius: 11,
    padding: 10,
    borderWidth: 1,
    borderColor: "#CDCDCD",
  },
});
