import React, {Component} from "react";
import {Text, View, StyleSheet, TextInput} from "react-native";
import {TouchableOpacity} from "react-native-gesture-handler";

const App = ({navigation}) => {
  return (
    <View style={{backgroundColor: "white", paddingBottom: 500}}>
      <Text style={styles.Judul}>Daftar</Text>
      <Text style={styles.SubJudul}>Username</Text>
      <TextInput
        placeholder="Masukan Kata Sandi"
        style={styles.TextInput}
      ></TextInput>
      <Text style={styles.SubJudul2}>Email</Text>
      <TextInput
        placeholder="Masukan Email"
        style={styles.TextInput}
      ></TextInput>
      <Text style={styles.SubJudul2}>Kata Sandi</Text>
      <TextInput
        placeholder="Masukan Kata  Sandi"
        style={styles.TextInput}
      ></TextInput>
      <Text style={styles.lupaKataSandi}>Lupa kata sandi?</Text>
      <TouchableOpacity
        onPress={() => navigation.navigate("home")}
        style={{flexDirection: "row"}}
      >
        <View style={styles.btnMulai}>
          <Text
            style={{color: "#FFF", padding: 16, paddingLeft: 107, fontSize: 18}}
          >
            Daftar
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate("masuk")}
        style={{flexDirection: "row"}}
      >
        <Text style={{marginTop: 10, color: "#c9c9c9", marginHorizontal: 84}}>
          Sudah punya akun? <Text style={{color: "#008EC5"}}>Masuk</Text>
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  Judul: {
    fontFamily: "Poppins-Bold",
    fontSize: 32,
    color: "#1F1F1F",
    marginTop: 40,
    marginLeft: 20,
  },
  SubJudul: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#828282",
    marginTop: 17,
    marginLeft: 44,
  },
  SubJudul2: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#828282",
    marginTop: 24,
    marginLeft: 44,
  },
  TextInput: {
    backgroundColor: "#F4F4F4",
    width: 292,
    height: 55,
    marginLeft: 34,
    borderRadius: 11,
    padding: 10,
    borderWidth: 1,
    borderColor: "#CDCDCD",
  },
  lupaKataSandi: {
    marginLeft: 236,
    marginTop: 6,
    color: "#828282",
    fontSize: 12,
  },
  btnMulai: {
    backgroundColor: "#080036",
    width: 264,
    height: 59,
    marginTop: 56,
    borderRadius: 20,
    marginHorizontal: 48,
  },
});

export default App;
