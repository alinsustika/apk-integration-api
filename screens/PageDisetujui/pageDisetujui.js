import React, {Component} from "react";
import Dash from "react-native-dash";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  YellowBox,
  TextInput,
  ScrollView,
} from "react-native";

export default class HomeActivity extends Component {
  constructor(props) {
    super(props);
    YellowBox.ignoreWarnings([
      "Warning: isMounted(...) is deprecated",
      "Module RCTImageLoader",
    ]);
  }

  render() {
    return (
      <ScrollView style={{backgroundColor: "white"}}>
        <View style={{marginLeft: 25, backgroundColor: "white"}}>
          <Text
            style={[styles.text, {fontSize: 14, marginLeft: 50, marginTop: 20}]}
          >
            Riwayat Pengajuan Disetujui
          </Text>
          <Text
            style={[styles.text, {marginLeft: 8, marginTop: 20, fontSize: 30}]}
          >
            Disetujui
          </Text>
          <Text
            style={[styles.text, {marginLeft: 8, marginTop: 0, fontSize: 14}]}
          >
            Pengajuanmu dengan nomor berikut, telah disetujui
          </Text>
          <Text
            style={[
              styles.text,
              {
                marginLeft: 8,
                marginTop: 0,
                fontSize: 30,
                fontFamily: "Poppins-SemiBold",
              },
            ]}
          >
            #100
          </Text>
          <Text
            style={[styles.text, {marginLeft: 8, marginTop: 0, fontSize: 14}]}
          >
            Silakan mengambil dana pada bagian keuangan pada jam kerja
          </Text>
          <Dash
            style={{
              width: 300,
              height: 5,
              borderRadius: 100,
              overflow: "hidden",
              dashColor: "#3005271",
              marginTop: 20,
            }}
          />
          <View style={{flexDirection: "row"}}>
            <Text style={styles.status}>Senin - Jumat</Text>
            <Text
              style={[
                styles.status,
                {marginLeft: 90, fontSize: 12, margin: 15},
              ]}
            >
              08:00 - 17:00
            </Text>
          </View>
          <Dash
            style={{
              width: 300,
              height: 5,
              borderRadius: 100,
              overflow: "hidden",
              dashColor: "#3005271",
              marginTop: 8,
              marginBottom: 20,
            }}
          />
          <Text
            style={[
              styles.text,
              {
                marginLeft: 8,
                marginTop: 0,
                fontSize: 14,
                fontFamily: "Poppins-Regular",
                color: "#848484",
              },
            ]}
          >
            Jika ada kendala, silakan hubungi bagian keuangan di 022 - 312 (
            nomor bebas pulsa ). Terimakasih.
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headerText: {
    padding: 15,
    borderRadius: 12,
    fontSize: 14,
    marginLeft: 10,
    fontFamily: "Poppins-Regular",
    backgroundColor: "#F1F1F1",
  },
  menuContent: {
    fontFamily: "Poppins-Regular",
    color: "#000",
    padding: 2,
    fontSize: 14,
  },
  text: {
    fontFamily: "Poppins-Regular",
    fontSize: 30,
    color: "#1F1F1F",
  },
  txtInput: {
    fontFamily: "Poppins-Regular",
    backgroundColor: "#f1f1f1",
    borderRadius: 12,
    marginLeft: 10,
    height: 50,
    padding: 15,
    fontSize: 14,
  },
  txtInputDeskripsi: {
    fontFamily: "Poppins-Regular",
    backgroundColor: "#f1f1f1",
    borderRadius: 12,
    marginLeft: 10,
    height: 200,
    padding: 15,
    fontSize: 14,
  },
  ambilFoto: {
    width: 120,
    height: 100,
    backgroundColor: "#fff",
    borderRadius: 12,
    padding: 10,
    textAlign: "center",
    alignContent: "center",
    alignItems: "center",
    justifyContent: "space-around",
    marginLeft: 24,
    marginTop: 8,
    elevation: 3.5,
  },
  status: {
    marginTop: 10,
    marginLeft: 10,
    fontFamily: "Poppins-Regular",
    fontSize: 16,
  },
});
