import React, {Component} from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import backIcon from "../../assets/backIcon.png";

// import notif from '../../assets/img/notif.png';
// import profil from '../../assets/img/man.jpg';
// import cardHome from '../../assets/img/ch.png';
// import Icon from 'react-native-vector-icons/Ionicons';
// import Lihat from '../../assets/img/chevron-forward-outline.svg';
// import { assets } from './react-nataive.config';

const Klaim = ({navigation}) => {
  return (
    <ScrollView>
      <View style={{backgroundColor: "white"}}>
        <View>
          <TouchableOpacity
            onPress={() => navigation.navigate("home")}
            style={{flexDirection: "row"}}
          >
            <View style={styles.buttonWrapper}>
              <Image source={backIcon} style={styles.img} />
            </View>
            <Text
              style={[
                styles.text,
                {fontSize: 14, marginLeft: 14, marginTop: 42},
              ]}
            >
              Status
            </Text>
          </TouchableOpacity>
          <Text
            style={[
              styles.text,
              {fontSize: 30, marginLeft: 24, justifyContent: "center"},
            ]}
          >
            Detail {"\n"}Status Klaim
          </Text>
        </View>
        <View style={{marginbottom: 50}}>
          <TouchableOpacity
            onPress={() => navigation.navigate("detailPengajuanDiproses")}
          >
            <View style={styles.cardStatus}>
              <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>

              <Text style={[styles.cardStatusKolom2, {color: "#F2C94C"}]}>
                Lihat Detail
              </Text>

              <Text style={styles.cardStatusKolom1}>#100</Text>
              <Text style={styles.cardStatusKolom2}>Rp400.000</Text>
              <Text style={styles.cardStatusKolom1}>10 Desember 2020</Text>
              <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
                Proses
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate("detailPengajuanSelesai")}
          >
            <View style={styles.cardStatus}>
              <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>
              <Text style={[styles.cardStatusKolom2, {color: "#F2C94C"}]}>
                Lihat Detail
              </Text>
              <Text style={styles.cardStatusKolom1}>#100</Text>
              <Text style={styles.cardStatusKolom2}>Rp400.000</Text>
              <Text style={styles.cardStatusKolom1}>10 Desember 2020</Text>
              <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
                Proses
              </Text>
            </View>
          </TouchableOpacity>

          <View style={styles.cardStatus}>
            <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>
            <Text style={[styles.cardStatusKolom2, {color: "#F2C94C"}]}>
              Lihat Detail
            </Text>
            <Text style={styles.cardStatusKolom1}>#100</Text>
            <Text style={styles.cardStatusKolom2}>Rp400.000</Text>
            <Text style={styles.cardStatusKolom1} ac>
              10 Desember 2020
            </Text>
            <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
              Proses
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  img: {
    marginTop: 40,
    width: 24,
    height: 24,
    marginLeft: 20,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 40,
  },
  notif: {
    width: 16,
    height: 19,
    marginLeft: 24,
    marginTop: 54,
  },
  profil: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginLeft: 250,
    marginTop: 35,
  },
  salam: {
    fontFamily: "Poppins-Bold",
    fontSize: 32,
    color: "#1F1F1F",
    marginTop: 10,
    marginLeft: 20,
  },
  cardHome: {
    marginTop: 1,
    width: 308,
    height: 190,
    // borderRadius: 50,
    marginLeft: -7,
  },
  cardPengajuan: {
    backgroundColor: "#fff",
    width: 248,
    height: 56,
    borderRadius: 18,
    marginLeft: 26,
    flexDirection: "row",
    alignItems: "center",
  },
  status: {
    marginTop: 10,
    marginLeft: 24,
    fontFamily: "Poppins-Regular",
    fontSize: 16,
  },
  cardStatus: {
    flexDirection: "column",
    marginLeft: 24,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 308,
    height: 108,
  },
  cardStatusKolom1: {
    marginLeft: 10,
    marginTop: 10,
    color: "black",
  },
  cardStatusKolom2: {
    marginLeft: 220,
    marginTop: -20,
  },
});

export default Klaim;
