import React, {Component} from "react";
import {Image, Text, View, StyleSheet, TextInput, Button} from "react-native";
import {TouchableOpacity} from "react-native-gesture-handler";
// import Icon from 'react-native-ionicons';

const App = ({navigation}) => {
  return (
    <View style={{backgroundColor: "white", paddingBottom: 500}}>
      <Text style={styles.Judul}>Masuk</Text>
      <Text style={styles.SubJudul}>Username / Email</Text>
      <TextInput
        placeholder="Masukan Username / Email"
        style={styles.TextInput}
      ></TextInput>
      <Text style={styles.SubJudul2}>Kata Sandi</Text>
      <TextInput
        placeholder="Masukan Kata  Sandi"
        style={styles.TextInput}
      ></TextInput>
      <Text style={styles.lupaKataSandi}>Lupa kata sandi?</Text>
      <View style={styles.btnMulai}>
        <TouchableOpacity
          onPress={() => navigation.navigate("home")}
          style={{flexDirection: "row"}}
        >
          <Text
            style={{color: "#FFF", padding: 16, paddingLeft: 107, fontSize: 18}}
          >
            Mulai
          </Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate("daftar")}
        style={{flexDirection: "row"}}
      >
        <Text style={{marginTop: 10, color: "#c9c9c9", marginHorizontal: 84}}>
          Belum punya akun? <Text style={{color: "#008EC5"}}>Daftar</Text>
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  Judul: {
    fontFamily: "Poppins-Bold",
    fontSize: 32,
    color: "#1F1F1F",
    marginTop: 40,
    marginLeft: 20,
  },
  SubJudul: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#828282",
    marginTop: 83,
    marginLeft: 44,
  },
  SubJudul2: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#828282",
    marginTop: 24,
    marginLeft: 44,
  },
  TextInput: {
    backgroundColor: "#F4F4F4",
    width: 292,
    height: 55,
    marginLeft: 34,
    borderRadius: 11,
    padding: 10,
    borderWidth: 1,
    borderColor: "#CDCDCD",
  },
  lupaKataSandi: {
    marginLeft: 236,
    marginTop: 6,
    color: "#828282",
    fontSize: 12,
  },
  btnMulai: {
    backgroundColor: "#080036",
    width: 264,
    height: 59,
    marginTop: 128,
    borderRadius: 20,
    marginHorizontal: 48,
  },
});

export default App;
