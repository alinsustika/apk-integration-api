import React, {Component} from "react";
import {
  Image,
  Text,
  View,
  StyleSheet,
  TextInput,
  Button,
  ScrollView,
} from "react-native";
import {TouchableOpacity} from "react-native-gesture-handler";
// import Icon from 'react-native-ionicons';
import backIcon from "../../../assets/backIcon.png";
import profil from "../../../assets/man.jpg";

const App = ({navigation}) => {
  return (
    <ScrollView>
      <View style={{backgroundColor: "white", paddingBottom: 350}}>
        <TouchableOpacity
          onPress={() => navigation.navigate("home")}
          style={{flexDirection: "row"}}
        >
          <View style={styles.buttonWrapper}>
            <Image source={backIcon} style={styles.img} />
          </View>
          <Text
            style={[styles.text, {fontSize: 14, marginLeft: 14, marginTop: 42}]}
          >
            Akun
          </Text>
          <Text
            style={[
              styles.text,
              {
                fontSize: 14,
                marginLeft: 224,
                marginTop: 42,
                color: "#FFC200",
                fontWeight: "bold",
              },
            ]}
          >
            Edit
          </Text>
        </TouchableOpacity>
        <View style={{flexDirection: "row"}}>
          <Image source={profil} style={styles.profil} />
        </View>

        <Text style={styles.SubJudul}>Nama</Text>
        <TextInput
          placeholder="Masukan Kata  Sandi"
          style={styles.TextInput}
        ></TextInput>
        <Text style={styles.SubJudul2}>Email</Text>
        <TextInput
          placeholder="Masukan Kata  Sandi"
          style={styles.TextInput}
        ></TextInput>
        <TextInput
          placeholder="Masukan Kata  Sandi"
          style={styles.TextInput}
        ></TextInput>
        <Text style={[styles.SubJudul2, {color: "#323338", marginLeft: 155}]}>
          Logout
        </Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  img: {
    marginTop: 40,
    width: 24,
    height: 24,
    marginLeft: 10,
  },
  profil: {
    width: 150,
    height: 150,
    borderRadius: 200,
    marginLeft: 100,
    marginTop: 20,
  },
  Judul: {
    fontFamily: "Poppins-Bold",
    fontSize: 32,
    color: "#1F1F1F",
    marginTop: 40,
    marginLeft: 20,
  },
  SubJudul: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#828282",
    marginTop: 40,
    marginLeft: 44,
  },
  SubJudul2: {
    fontFamily: "Poppins-Regular",
    fontSize: 14,
    color: "#828282",
    marginTop: 24,
    marginLeft: 44,
  },
  TextInput: {
    backgroundColor: "#F4F4F4",
    width: 292,
    height: 55,
    marginLeft: 34,
    borderRadius: 11,
    padding: 10,
    borderWidth: 1,
    borderColor: "#CDCDCD",
  },
  lupaKataSandi: {
    marginLeft: 236,
    marginTop: 6,
    color: "#828282",
    fontSize: 12,
  },
  btnMulai: {
    backgroundColor: "#080036",
    width: 264,
    height: 59,
    marginTop: 128,
    borderRadius: 20,
    marginHorizontal: 48,
  },
});

export default App;
