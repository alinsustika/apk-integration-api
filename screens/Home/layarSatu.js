import React from "react";
import {View, Text, Button} from "react-native";

export default function layarSatu() {
  return (
    <ScrollView>
      <View>
        <View style={{flexDirection: "row"}}>
          <Image source={notif} style={styles.notif} />
          <Image source={profil} style={styles.profil} />
        </View>
        <Text style={styles.salam}>Hai, Faiz</Text>
        <View style={styles.container}>
          <ImageBackground source={cardHome} style={styles.cardHome}>
            <Text style={[styles.text, {color: "#FFFFFF", marginTop: 16}]}>
              085712345678
            </Text>
            <Text
              style={[
                styles.text,
                {fontSize: 24, color: "#FFFFFF", marginTop: 5},
              ]}
            >
              Ajukan Klaim
            </Text>
            <View style={styles.cardPengajuan}>
              <Text style={[{color: "#1F1F1F", padding: 10}]}>
                Riwayat Pengajuan{" "}
              </Text>
              {/* <Icon name="home-outline" size={10} /> */}
            </View>
          </ImageBackground>
        </View>
        <View style={{flexDirection: "row"}}>
          <Text style={styles.status}>Status</Text>
          <Text
            style={[
              styles.status,
              {marginLeft: 180, fontSize: 12, marginTop: 14},
            ]}
          >
            lihat semua
          </Text>
        </View>
        <View style={{marginbottom: 50}}>
          <View style={styles.cardStatus}>
            <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>
            <Text style={[styles.cardStatusKolom2, {color: "#F2C94C"}]}>
              Lihat Detail
            </Text>
            <Text style={styles.cardStatusKolom1}>#100</Text>
            <Text style={styles.cardStatusKolom2}>Rp400.000</Text>
            <Text style={styles.cardStatusKolom1}>10 Desember 2020</Text>
            <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
              Proses
            </Text>
          </View>
          <View style={styles.cardStatus}>
            <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>
            <Text style={[styles.cardStatusKolom2, {color: "#F2C94C"}]}>
              Lihat Detail
            </Text>
            <Text style={styles.cardStatusKolom1}>#100</Text>
            <Text style={styles.cardStatusKolom2}>Rp400.000</Text>
            <Text style={styles.cardStatusKolom1}>10 Desember 2020</Text>
            <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
              Proses
            </Text>
          </View>
          <View style={styles.cardStatus}>
            <Text style={styles.cardStatusKolom1}>ID Pengajuan</Text>
            <Text style={[styles.cardStatusKolom2, {color: "#F2C94C"}]}>
              Lihat Detail
            </Text>
            <Text style={styles.cardStatusKolom1}>#100</Text>
            <Text style={styles.cardStatusKolom2}>Rp400.000</Text>
            <Text style={styles.cardStatusKolom1} ac>
              10 Desember 2020
            </Text>
            <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
              Proses
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 40,
  },
  notif: {
    width: 16,
    height: 19,
    marginLeft: 24,
    marginTop: 54,
  },
  profil: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginLeft: 250,
    marginTop: 35,
  },
  salam: {
    fontFamily: "Poppins-Bold",
    fontSize: 32,
    color: "#1F1F1F",
    marginTop: 10,
    marginLeft: 20,
  },
  cardHome: {
    marginTop: 1,
    width: 308,
    height: 190,
    // borderRadius: 50,
    marginLeft: -7,
  },
  cardPengajuan: {
    backgroundColor: "#fff",
    width: 248,
    height: 56,
    borderRadius: 18,
    marginLeft: 26,
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10,
  },
  status: {
    marginTop: 10,
    marginLeft: 24,
    fontFamily: "Poppins-Regular",
    fontSize: 16,
  },
  cardStatus: {
    flexDirection: "column",
    marginLeft: 24,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 308,
    height: 108,
  },
  cardStatusKolom1: {
    marginLeft: 10,
    marginTop: 10,
    color: "black",
  },
  cardStatusKolom2: {
    marginLeft: 220,
    marginTop: -20,
  },
  text: {
    marginLeft: 30,
  },
});

// import React from 'react'
// import { View, Text, Button } from 'react-native'

// export default function layarSatu({navigation}) {
//     return (
//         <View style={{flex:1}}>
//             <Text style={{
//                 color:'red',
//                 fontSize:50,
//                 alignItems:'center',
//                 justifyContent:'center'
//             }}>Ini di Layar Satu</Text>
//             <Button
//                 title = "Berpindah ke Halaman Dua"
//                 onPress={()=>navigation.navigate('Dua',{
//                     asal:'Layar Satu'
//                 })}
//             />
//         </View>
//     )
// }
