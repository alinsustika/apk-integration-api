import React, {Component} from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  YellowBox,
  TextInput,
  ScrollView,
  ImageBackground,
  Image,
  TouchableOpacity,
} from "react-native";
import backIcon from "../../../assets/backIcon.png";

const Riwayat = ({navigation}) => {
  // constructor(props) {
  //    super(props);
  //    YellowBox.ignoreWarnings([
  //     'Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'
  //   ]);
  //  }

  return (
    <ScrollView>
      <View style={{marginLeft: 25, backgroundColor: "white"}}>
        <TouchableOpacity
          onPress={() => navigation.navigate("home")}
          style={{flexDirection: "row"}}
        >
          <View style={styles.buttonWrapper}>
            <Image source={backIcon} style={styles.img} />
          </View>
          <Text
            style={[styles.text, {fontSize: 14, marginLeft: 14, marginTop: 40}]}
          >
            Riwayat
          </Text>
        </TouchableOpacity>

        <Text
          style={[styles.text, {marginLeft: 8, marginTop: 20, fontSize: 30}]}
        >
          Riwayat Pengajuan
        </Text>
        <View style={{flexDirection: "row", justifyContent: "space-around"}}>
          <Text style={styles.status}>Hari Ini</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate("riwayat_mingguIni")}
            style={{flexDirection: "row"}}
          >
            <Text style={[styles.status, {color: "#C0C0C0"}]}>Minggu Ini</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate("riwayat_bulanIni")}
            style={{flexDirection: "row"}}
          >
            <Text style={[styles.status, {color: "#C0C0C0"}]}>Bulan Ini</Text>
          </TouchableOpacity>
        </View>

        <View style={{marginbottom: 50, marginLeft: -24}}>
          <View style={styles.cardStatus}>
            <Text style={styles.cardStatusKolom1}>#100</Text>
            <Text style={styles.cardStatusKolom1}>Klaim Medis</Text>
            <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
              Rp300.000
            </Text>
            <Text style={styles.cardStatusKolom1}>10 Des 2020</Text>
          </View>
          <View style={styles.cardStatus}>
            <Text style={styles.cardStatusKolom1}>#100</Text>
            <Text style={styles.cardStatusKolom1}>Klaim Medis</Text>
            <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
              Rp300.000
            </Text>
            <Text style={styles.cardStatusKolom1}>10 Des 2020</Text>
          </View>
          <View style={styles.cardStatus}>
            <Text style={styles.cardStatusKolom1}>#100</Text>
            <Text style={styles.cardStatusKolom1}>Klaim Medis</Text>
            <Text style={[styles.cardStatusKolom2, {fontWeight: "bold"}]}>
              Rp300.000
            </Text>
            <Text style={styles.cardStatusKolom1}>10 Des 2020</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  img: {
    marginTop: 40,
    width: 24,
    height: 24,
  },
  cardHome: {
    marginTop: 1,
    width: 308,
    height: 190,
  },
  text: {
    color: "#1F1F1F",
    marginLeft: 26,
    marginTop: 12,
    fontSize: 12,
    fontFamily: "Poppins-Regular",
  },
  status: {
    marginTop: 10,
    marginLeft: -25,
    fontFamily: "Poppins-Regular",
    fontSize: 14,
  },
  cardStatus: {
    flexDirection: "column",
    marginLeft: 24,
    marginTop: 14,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: "#BFBFBF",
    width: 308,
    height: 108,
  },
  cardStatusKolom1: {
    marginLeft: 10,
    marginTop: 10,
    color: "black",
  },
  cardStatusKolom2: {
    marginLeft: 220,
    marginTop: -20,
  },
});

export default Riwayat;
